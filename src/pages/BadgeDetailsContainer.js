import React from 'react'

import api from '../api'
import PageLoading from '../components/PageLoading'
import PageError from '../components/PageError'
import BadgesDetails from '../pages/BadgeDetails'

class BadgeDetailsContainer extends React.Component{
    state= {
        loading: true,
        error: null,
        data:undefined,
        modalIsOpen: false
    }

    componentDidMount(){
        this.fetchData()
    }

    fetchData = async () => {
        this.setState({loading: true, error: null})

        try{
            const data = await api.badges.read(this.props.match.params.badgesId)
            this.setState({loading:false, error: null, data: data}) 
        }catch(error){
            this.setState({loading:false, error: error})
        }
    }

    handleOpenModal = event => {
        this.setState({modalIsOpen: true})
    }

    handleCloseModal = event => {
        this.setState({modalIsOpen: false})
    }

    handleDeleteBadge = async event => {
        this.setState({loading: true, error: null})

        try{
            await api.badges.remove(this.props.match.params.badgesId)
            this.props.history.push('/badges')
        }catch(error){
            this.setState({loading:false, error: error})
        }
    }

    render(){
        if(this.state.loading){
            return <PageLoading/>
        }
        if(this.state.error){
            return <PageError error={this.state.error}/>
        }

        return(
            <BadgesDetails 
                onCloseModal={this.handleCloseModal}
                onOpenModal={this.handleOpenModal}
                modalIsOpen={this.state.modalIsOpen}
                onDeleteBadge={this.handleDeleteBadge}
                badge={this.state.data}/>
        )
    }
}

export default BadgeDetailsContainer