import React from 'react'
import { Link } from 'react-router-dom'

import './style/Home.css'

import confLogo from '../images/platziconf-logo.svg'
import logo from '../images/astronauts.svg'

class Home extends React.Component {
    render(){
        return (
            <div className="Home__background">
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <img src={confLogo} alt="confColor"/>
                            <h1 className="Home__color-text" >Welcome to the conference</h1>
                            <Link to="/badges" className="btn btn-primary">
                                Look at the badges 
                            </Link>
                            <Link to="/badges/new" className="btn btn-primary">
                                Create your badge
                            </Link>
                        </div>
                        <div className="col-6">
                            <img className="Home__image-logo" src={logo} alt="logo"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home