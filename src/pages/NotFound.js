import React from 'react'

import './style/NotFound.css'
import imageAstronaut from '../images/404NotFound.png'
import { Link } from 'react-router-dom'

function NotFound(){
    return <div className="NotFound__background">
        <div className="container">
            <div className="row">
                <div className="col-7">
                    <img className="NotFound__image-logo" src={imageAstronaut} alt="astronauta"/>
                </div>
                <div className="col-5">
                    <h1 className="Notfound__404-text">404</h1>
                    <h1 className="Notfound__title">Page not found</h1>
                    <h2 className="NotFound__color-text">Oooops!! Something went wrong. Go back to Home</h2>
                    <Link to="/" className="btn btn-primary">
                        Back to Home
                    </Link>
            </div>
        </div>
    </div>
        
    </div>
    
}

export default NotFound