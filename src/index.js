import React from 'react' //análogo de createElement
import ReactDOM from 'react-dom' // análogo a appendChild

import 'bootstrap/dist/css/bootstrap.css'
import "./global.css"

import App from './components/App'

const container = document.getElementById('app')

ReactDOM.render(<App /> , container)