import React from 'react'
import './styles/PageLoading.css'
import Loader from './Loader'

class PageLoading extends React.Component{
    render(){
        return <div>
            <div className="PageLoading">
                <Loader/>
            </div>
        </div>
    }
}

export default PageLoading