import React from 'react'

class BadgeForm extends React.Component {
    //Método evento del botón
    handleClick = (event) => {
        console.log(
            "Button was clicked"
        )
    }

    //Método evento del formulario
    handleSumit = event =>{
        console.log("Form was submmit")
        //Si no queremos que el formulario se envíe
        event.preventDefault();
    }

    render(){
        return <React.Fragment>
            <form onSubmit={this.props.onSubmit} >
                <div onChange={this.props.onChange} className="form-group">
                    <label htmlFor="">First Name</label>
                    <input className="form-control" type="text" name="firstName" 
                    defaultValue={this.props.formValues.firstName}/>
                </div>

                <div onChange={this.props.onChange} className="form-group">
                    <label htmlFor="">Last Name</label>
                    <input  className="form-control" type="text" name="lastName"
                    defaultValue={this.props.formValues.lastName}/>
                </div>
                <div onChange={this.props.onChange} className="form-group">
                    <label htmlFor="">Email</label>
                    <input  className="form-control" type="email" name="email"
                    defaultValue={this.props.formValues.email}/>
                </div>
                <div onChange={this.props.onChange} className="form-group">
                    <label htmlFor="">Job Title</label>
                    <input  className="form-control" type="text" name="jobTitle"
                    defaultValue={this.props.formValues.jobTitle}/>
                </div>
                <div onChange={this.props.onChange} className="form-group">
                    <label htmlFor="">Twitter</label>
                    <input  className="form-control" type="text" name="twitter"
                    defaultValue={this.props.formValues.twitter}/>
                </div>

                <button onClick={this.handleClick} className="btn btn-primary">Save</button>
                {this.props.error && (
                    <p className="text-danger">{this.props.error.message}</p>
                )}
            </form>
        </React.Fragment>

    }
}

export default BadgeForm