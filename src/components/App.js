import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import BagdeNew from '../pages/BadgeNew'
import Badges from '../pages/Badges'
import Layout from './Layout'
import Home from '../pages/Home'
import NotFound from '../pages/NotFound'
import BadgeEdit from '../pages/BadgeEdit'
import BadgeDetails from '../pages/BadgeDetailsContainer'


function App() {
    return (
    <BrowserRouter>
        <Layout>
            <Switch >
                <Route exact path="/badges" component={Badges} />
                <Route exact path="/badges/new" component={BagdeNew} />
                <Route exact path="/badges/:badgesId/edit" component={BadgeEdit} />
                <Route exact path="/badges/:badgesId" component={BadgeDetails} />
                <Route exact path="/" component={Home} />
                <Route component={NotFound} />
            </Switch>    
        </Layout>
    </BrowserRouter>
    )
}

export default App