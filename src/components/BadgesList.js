import React from 'react'
import { Link } from 'react-router-dom'
import './styles/BadgesList.css'
import Gravatar from '../components/Gravatar'

class BadgesListItem extends React.Component{
    render(){
        return <React.Fragment> 
        <div className="BadgesListItem">
            <Gravatar 
                className="BadgesListItem__avatar"
                email={this.props.badges.email}
                alt="avatar"
            />
            <div>
                <strong>
                    {this.props.badges.firstName} {this.props.badges.lastName}
                </strong>
                <br/> @{this.props.badges.twitter}
                <br/> {this.props.badges.jobTitle}
            </div>
        </div>
        </React.Fragment>
    }
}

//Custom hooks

function useSearchBadges(badges){
    const [ query, setQuery] = React.useState("")
    const [ filteredBadges, setFilteredBadges] = React.useState(badges)

    React.useMemo(() => {
        const result = badges.filter(badge => {
            return `${badge.firstName} ${badge.lastName}`.toLowerCase().includes(query.toLowerCase())
        })

        setFilteredBadges(result)

    }, [badges, query])

return { query, setQuery, filteredBadges}
}

function BadgesList (props){
    const badges = props.badges
    //Utilizamos el hook customizado
    const { query, setQuery, filteredBadges} = useSearchBadges(badges)

    if(filteredBadges.length === 0) {
        return (
            <div>
                <div className="form-group">
                    <label>Filter Badges</label>
                    <input type="text" className="form-control"
                    value={query}
                    onChange={(e) => {
                        setQuery(e.target.value)
                        }}
                    />
                </div>
                <h3>No badges were found</h3>
                <Link to={"badges/new"} className="btn btn-primary">
                    Create new badge
                </Link>
            </div>
        )
    }

    return <div>
        <div className="form-group">
            <label>Filter Badges</label>
            <input type="text" className="form-control"
                value={query}
                onChange={(e) => {
                    setQuery(e.target.value)
                }}
            />
        </div>
        <ul className="list-unstyled">
            {filteredBadges.map((badge) => {
                return <li key={badge.id}>
                    <p>
                        <Link className="text-reset text-decoration-none" to={`/badges/${badge.id}`}>
                            <BadgesListItem badges={badge}/>
                        </Link>
                    </p>
                </li>
            })}
        </ul>
    </div>
}

export default BadgesList